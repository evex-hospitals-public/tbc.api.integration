﻿using System.Security.Cryptography.X509Certificates;
using System.Web.Services.Protocols;
using PaymentService;

namespace TBC_Integration.NETFramework
{
    class Program
    {
        static void Main(string[] args)
        {
            var ps = new PaymentService.PaymentService();

            addSecurityHeader(ps);
            AttachCertificate(ps);
            ps.Url = "https://secdbitst.tbconline.ge/dbi/dbiService";

            //var res = ps.GetPaymentOrderStatus(new GetPaymentOrderStatusRequestIo());

            var paymentOrder = createSamplePaymentOrder();
            var paymentOrders = new TransferToOwnAccountPaymentOrderIo[1] {paymentOrder};
            var paymentOrderResult = ps.ImportSinglePaymentOrders(paymentOrders);
            System.Diagnostics.Debug.WriteLine("Resulting paymentId: {0}",
                paymentOrderResult[0].paymentId);
        }

        private static TransferToOwnAccountPaymentOrderIo createSamplePaymentOrder()
        {
            var paymentOrder = new TransferToOwnAccountPaymentOrderIo();
            paymentOrder.creditAccount = new AccountIdentificationIo();
            paymentOrder.creditAccount.accountNumber = "GE86TB1144836120100002";
            paymentOrder.creditAccount.accountCurrencyCode = "USD";
            paymentOrder.debitAccount = new AccountIdentificationIo();
            paymentOrder.debitAccount.accountNumber = "GE69TB1144836020100001";
            paymentOrder.debitAccount.accountCurrencyCode = "GEL";
            paymentOrder.amount = new MoneyIo();
            paymentOrder.amount.amount = 1;
            paymentOrder.amount.currency = "USD";
            paymentOrder.description = "my best description";
            return paymentOrder;
        }

        private static void addSecurityHeader(PaymentService.PaymentService ps)
        {
            ps.secHeader = new PaymentService.PaymentService.Security();
            ps.secHeader.UsernameToken = new PaymentService.PaymentService.UsernameToken();
            ps.secHeader.UsernameToken.Username = "test";
            ps.secHeader.UsernameToken.Password = "PASSWORD";
            ps.secHeader.UsernameToken.Nonce = "1111";
        }

        private static HttpWebClientProtocol AttachCertificate(HttpWebClientProtocol req)
        {
            string certPath = "SecDbiTst.tbconline.ge.pfx";
            const string certPass = "fek4Ng59";

            X509Certificate2Collection collection = new X509Certificate2Collection();
            collection.Import(certPath, certPass, X509KeyStorageFlags.PersistKeySet);
            //X509Certificate cert = collection[1];
            req.ClientCertificates.AddRange(collection);
            return req;
        }
    }
}